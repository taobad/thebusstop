package southampton.badmus.com.thebusstop;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Badmus on 28/12/2015.
 */
public class BusStop implements Comparable<BusStop>, Parcelable{

    private String name;
    private Float distance;
    private Location location;
    private List<Bus> buses;
    private String message;

    public BusStop() {
        this.name = "";
        this.distance = 0.0f;
        this.buses = new ArrayList<Bus>();
        this.message = "";
    }

    public BusStop(List<Bus> buses, Float distance, String name, String message) {
        this.buses = buses;
        this.distance = distance;
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public List<Bus> getBuses() {
        return buses;
    }

    public void setBuses(List<Bus> buses) {
        this.buses = buses;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int compareTo(BusStop busStop) {
        return this.distance.intValue() - busStop.distance.intValue();
    }

    public static Comparator<BusStop> BusStopComparator
            = new Comparator<BusStop>() {

        public int compare(BusStop bs1, BusStop bs2) {

            Float distance1 = bs1.getDistance();
            Float distance2 = bs2.getDistance();

            //ascending order
            return distance1.compareTo(distance2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeValue(this.distance);
        dest.writeParcelable(this.location, 0);
        dest.writeTypedList(buses);
        dest.writeString(this.message);
    }

    protected BusStop(Parcel in) {
        this.name = in.readString();
        this.distance = (Float) in.readValue(Float.class.getClassLoader());
        this.location = in.readParcelable(Location.class.getClassLoader());
        this.buses = in.createTypedArrayList(Bus.CREATOR);
        this.message = in.readString();
    }

    public static final Creator<BusStop> CREATOR = new Creator<BusStop>() {
        public BusStop createFromParcel(Parcel source) {
            return new BusStop(source);
        }

        public BusStop[] newArray(int size) {
            return new BusStop[size];
        }
    };
}
