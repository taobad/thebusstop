package southampton.badmus.com.thebusstop;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class NearestStop extends AppCompatActivity {

    static Location mActiveLocation; //whether last known or current location, this represents the active location in use on the screen
    ListView busLV, busStopLV;
    ListAdapter busLA, busStopLA;
    TextView busStopTV;
    ImageView directions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearest_stop);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mActiveLocation = new Location("");
        mActiveLocation.setLatitude(50.920396);
        mActiveLocation.setLongitude(-1.395622);

        List<Bus> buses = new ArrayList<Bus>();
        Bus bus1 = new Bus();
        bus1.setName("Bus 1");
        bus1.setBusTime(new Date());
        bus1.setBusRoute("U6C- University to City Centre Via Portswood, HighfieldInterchange and Solent");
        buses.add(bus1);


        Bus bus2 = new Bus();
        bus2.setName("Bus 2");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DATE);
        System.out.println(year);
        Date date2 = new Date(year - 1900, month, day);
        bus2.setBusTime(date2);
        bus2.setBusRoute("U6C- University to City Centre Via Portswood, HighfieldInterchange and Solent");
        buses.add(bus2);

        Bus bus3 = new Bus();
        bus3.setName("Bus 3");
        bus3.setBusTime(new Date());
        bus3.setBusRoute("U6C- University to City Centre Via Portswood, HighfieldInterchange and Solent");
        buses.add(bus3);

        Bus bus4 = new Bus();
        bus4.setName("Bus 4");
        bus4.setBusTime(new Date());
        bus4.setBusRoute("U6C- University to City Centre Via Portswood, HighfieldInterchange and Solent");
        buses.add(bus4);

        BusStop stop1 = new BusStop();
        Location dummyLoc1 = new Location("");
        dummyLoc1.setLatitude(50.92735);
        dummyLoc1.setLongitude(-1.38921);
        stop1.setName("sainsbury");
        stop1.setLocation(dummyLoc1);
        stop1.setMessage("Buses to stop working for christmas");
        stop1.setBuses(buses);

        BusStop stop2 = new BusStop();
        Location dummyLoc2 = new Location("");
        dummyLoc2.setLatitude(50.92408);
        dummyLoc2.setLongitude(-1.39478);
        stop2.setName("Waitrose");
        stop2.setLocation(dummyLoc2);
        stop2.setMessage("Buses to stop working for New year");
        stop2.setBuses(buses);

        BusStop stop3 = new BusStop();
        Location dummyLoc3 = new Location("");
        dummyLoc3.setLatitude(50.936344);
        dummyLoc3.setLongitude(-1.39681);
        stop3.setName("Southampton university");
        stop3.setLocation(dummyLoc3);
        stop3.setMessage("Buses to stop working during Vacation");
        stop3.setBuses(buses);


        BusStop stop4 = new BusStop();
        Location dummyLoc4 = new Location("");
        dummyLoc4.setLatitude(50.938015401551);
        dummyLoc4.setLongitude(-1.3863872730648);
        stop4.setName("Borges Road Library");
        stop4.setLocation(dummyLoc4);
        stop4.setMessage("Buses to stop working for Boxing Day");
        stop4.setBuses(buses);

        List<BusStop> busStops = new ArrayList<BusStop>();
        busStops.add(stop1);
        busStops.add(stop2);
        busStops.add(stop3);
        busStops.add(stop4);

        computeDistances(busStops);
        Collections.sort(busStops, BusStop.BusStopComparator);
        final BusStop nearestBusStop = busStops.get(0);
        busStops.remove(0);
        busStopLA = new BusStopListAdapter(NearestStop.this, R.layout.nearby_busstops_listview_layout, busStops);

        busStopLV = (ListView) findViewById(R.id.nearByBusStops);
        busStopLV.setAdapter(busStopLA);

        busStopLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BusStop busStop = (BusStop) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(NearestStop.this, BusStopActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putParcelable("BusStop", busStop);
                intent.putExtras(mBundle);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, nearestBusStop.getMessage(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        busStopTV = (TextView) findViewById(R.id.busStopId);
        busStopTV.setText(nearestBusStop.getName());
        busStopTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NearestStop.this, BusStopActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putParcelable("BusStop", nearestBusStop);
                intent.putExtras(mBundle);
                startActivity(intent);
            }
        });


        busLA = new BusListAdapter(NearestStop.this, R.layout.listview_layout, nearestBusStop.getBuses());
        busLV = (ListView) findViewById(R.id.nearestBusStop);
        busLV.setAdapter(busLA);
        busLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bus bus = (Bus) adapterView.getItemAtPosition(i);
                Toast.makeText(NearestStop.this,bus.getBusRoute(),Toast.LENGTH_SHORT).show();
            }
        });

        directions = (ImageView) findViewById(R.id.directions);
        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NearestStop.this, MapActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putParcelable("BusStop", nearestBusStop);
                intent.putExtras(mBundle);
                startActivity(intent);
            }
        });

    }

    private void computeDistances(List<BusStop> busStop){
        for(BusStop bs : busStop){
            Float distance = round(mActiveLocation.distanceTo(bs.getLocation())/1000, 2);
            bs.setDistance(distance);
        }
    }

    public float round(float value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.floatValue();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nearest_stop, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class BusStopListAdapter extends ArrayAdapter<BusStop> {

        private List<BusStop> busStops;

        public BusStopListAdapter(Context context, int textViewResourceId, List<BusStop> busStops) {
            super(context, textViewResourceId, busStops);
            this.busStops = busStops;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            BusStop busSt = busStops.get(position);
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.nearby_busstops_listview_layout, null);


            }

            TextView busStop = (TextView) v.findViewById(R.id.busStop);
            TextView busStopDistance = (TextView) v.findViewById(R.id.busStopDistance);

            busStop.setText(busSt.getName());
            busStopDistance.setText(busSt.getDistance().toString()+"km");

            return v;
        }
    }

    private class BusListAdapter extends ArrayAdapter<Bus> {

        private List<Bus> buses;

        public BusListAdapter(Context context, int textViewResourceId, List<Bus> buses) {
            super(context, textViewResourceId, buses);
            this.buses = buses;
            Collections.sort(this.buses, Bus.BusComparator);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Bus bus = buses.get(position);
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.listview_layout, null);


            }

            TextView busTv = (TextView) v.findViewById(R.id.bus);
            TextView busTime = (TextView) v.findViewById(R.id.busTime);

            busTv.setText(bus.getName());
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            String time = sdf.format(bus.getBusTime());
            busTime.setText(time);

            return v;
        }
    }
}