package southampton.badmus.com.thebusstop;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by Badmus on 28/12/2015.
 */
public class Bus implements Comparable<Bus>, Parcelable {
    private String name;
    private Date busTime;
    private String busRoute;

    public Bus() {
        this.name = "";
        this.busRoute = "";
        this.busTime = new Date();
    }

    public Bus(String name, Date busTime) {
        this.name = name;
        this.busTime = busTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBusTime() {
        return busTime;
    }

    public void setBusTime(Date busTime) {
        this.busTime = busTime;
    }

    public String getBusRoute() {
        return busRoute;
    }

    public void setBusRoute(String busRoute) {
        this.busRoute = busRoute;
    }

    @Override
    public int compareTo(Bus bus) {
        return this.busTime.compareTo(bus.busTime);
    }

    public static Comparator<Bus> BusComparator
            = new Comparator<Bus>() {

        public int compare(Bus bs1, Bus bs2) {

            Date date1 = bs1.getBusTime();
            Date date2 = bs2.getBusTime();

            //ascending order
            return date1.compareTo(date2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }

    };



    /*public static final Parcelable.Creator<Bus> CREATOR = new Creator<Bus>() {
        public Bus createFromParcel(Parcel source) {
            Bus bus = new Bus();
            bus.name = source.readString();
            bus.busTime = (Date) source.readSerializable();

            return bus;
        }
        public Bus[] newArray(int size) {
            return new Bus[size];
        }
    };

    public int describeContents() {
        return 0;
    }
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(name);
        parcel.writeSerializable(busTime);
    }*/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeLong(busTime != null ? busTime.getTime() : -1);
        dest.writeString(this.busRoute);
    }

    protected Bus(Parcel in) {
        this.name = in.readString();
        long tmpBusTime = in.readLong();
        this.busTime = tmpBusTime == -1 ? null : new Date(tmpBusTime);
        this.busRoute = in.readString();
    }

    public static final Creator<Bus> CREATOR = new Creator<Bus>() {
        public Bus createFromParcel(Parcel source) {
            return new Bus(source);
        }

        public Bus[] newArray(int size) {
            return new Bus[size];
        }
    };
}