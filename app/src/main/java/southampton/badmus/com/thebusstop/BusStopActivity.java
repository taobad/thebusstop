package southampton.badmus.com.thebusstop;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

/**
 * Created by Badmus on 30/12/2015.
 */
public class BusStopActivity extends AppCompatActivity {
    ListView busLV;
    ListAdapter busLA;
    TextView busStopTV;
    ImageView directions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busstop);

        getSupportActionBar().hide();

        final BusStop busStop = (BusStop) getIntent().getParcelableExtra("BusStop");

        busStopTV = (TextView) findViewById(R.id.busStopId);
        busStopTV.setText(busStop.getName());

        busLA = new BusListAdapter(BusStopActivity.this, R.layout.listview_layout, busStop.getBuses());

        busLV = (ListView) findViewById(R.id.nearestBusStoppp);
        busLV.setAdapter(busLA);
        busLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bus bus = (Bus) adapterView.getItemAtPosition(i);
                Toast.makeText(BusStopActivity.this,bus.getBusRoute(),Toast.LENGTH_SHORT).show();
            }
        });

        directions = (ImageView) findViewById(R.id.directions);
        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BusStopActivity.this, MapActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putParcelable("BusStop", busStop);
                intent.putExtras(mBundle);
                startActivity(intent);
            }
        });

    }

    private class BusListAdapter extends ArrayAdapter<Bus> {

        private List<Bus> buses;

        public BusListAdapter(Context context, int textViewResourceId, List<Bus> buses) {
            super(context, textViewResourceId, buses);
            this.buses = buses;
            Collections.sort(this.buses, Bus.BusComparator);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Bus bus = buses.get(position);
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.listview_layout, null);


            }

            TextView buss = (TextView) v.findViewById(R.id.bus);
            TextView busTime = (TextView) v.findViewById(R.id.busTime);

            buss.setText(bus.getName());
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            String time = sdf.format(bus.getBusTime());
            busTime.setText(time);

            return v;
        }
    }
}