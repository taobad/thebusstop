package com.badmus.southampton.busstop.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Created by Badmus on 02/01/2016.
 */
@Entity
public class Bus {
    @Id
    private String name;
    private String route;

    public Bus() {
        this.name = "";
        this.route = "";
    }

    public Bus(String name, String route) {
        this.name = name;
        this.route = route;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }
}

