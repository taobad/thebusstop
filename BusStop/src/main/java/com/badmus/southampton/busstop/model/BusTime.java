package com.badmus.southampton.busstop.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.Date;
import java.util.List;

/**
 * Created by Badmus on 02/01/2016.
 */
@Entity
public class BusTime {
    @Id
    private Bus bus;
    private List<Date> times;
    private String Day;

}
