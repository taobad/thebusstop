package com.badmus.southampton.busstop.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Badmus on 02/01/2016.
 */
@Entity
public class BusCompany {

    @Id
    private String email;
    private String name;
    private String password;
    //private List<Bus> buses;

    public BusCompany() {
        this.email = "";
        this.name = "";
        this.password = "";
//        this.buses = new ArrayList<Bus>();
    }

    public BusCompany(String email,String password, String name) {
        this.email = email;
        this.name = name;
        this.password = password;
  //      this.buses = new ArrayList<Bus>();
    }

    public BusCompany(String name, String password, List<Bus> buses) {
        this.name = name;
        this.password = password;
    //    this.buses = buses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*public List<Bus> getBuses() {
        return buses;
    }

    public void setBuses(List<Bus> buses) {
        this.buses = buses;
    }*/
}
