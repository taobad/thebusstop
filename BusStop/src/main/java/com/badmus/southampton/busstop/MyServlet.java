/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package com.badmus.southampton.busstop;

import com.badmus.southampton.busstop.manager.BusCompanyManager;

import org.json.simple.JSONObject;

import java.io.IOException;
import javax.servlet.http.*;

public class MyServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/plain");
        resp.getWriter().println("Please use the form to POST to this url");
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String action = req.getParameter("action");

        if(action.equals("regcompany")){
            String password = req.getParameter("cp");
            String email = req.getParameter("ce");
            String companyName = req.getParameter("cn");

            String status_message = "";
            JSONObject object = new JSONObject();
            try {
                String status = new BusCompanyManager().registerBusCompany(email, password,companyName);
                if(status.equals("ok")){
                    //session = req.getSession();
                    //session.setMaxInactiveInterval(120);
                    //session.setAttribute("email",email);
					/*RequestDispatcher rd = req.getRequestDispatcher("cv.html");
			        rd.forward(req,  resp);*/
                    status_message = "ok";
                    object.put("status", status);
                    object.put("status_message", status_message);
                    resp.getWriter().println(object.toString());
                } else if (status.equals("13")){
                    status_message = "There is already an account registered under the provided email address. Please, try loggin in instead.";
                    object.put("status", status);
                    object.put("status_message", status_message);
                    resp.getWriter().println(object.toString());
                } else if(status.equals("14")){
                    status_message = "Unable to register your email address. Please, try again in a few minutes.";
                    object.put("status", status);
                    object.put("status_message", status_message);
                    resp.getWriter().println(object.toString());
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
