package com.badmus.southampton.busstop.manager;

import com.badmus.southampton.busstop.model.Bus;
import com.badmus.southampton.busstop.model.BusCompany;
import com.badmus.southampton.busstop.utility.OfyService;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;


/**
 * Created by Badmus on 02/01/2016.
 */

public class BusCompanyManager extends OfyService {
    public BusCompanyManager(){
        new OfyService();
    }

    /*public BusCompany getBusCompany(String email) {
        BusCompany busCompany = null;
        System.out.println("getting data for: " + email);
        try{
            busCompany = ofy().load().type(BusCompany.class).id(email).safe();
        } catch(Exception e){
            return busCompany;
        } finally{
            ofy().clear();
        }
        return busCompany;
    }*/

    public String registerBusCompany(String email, String password, String companyName){
        BusCompany busCompany = null;

        String em = email;
        String status = "";


        if ((email == null) || (email.trim().equals(""))){
            status = "11";
        }  else if ((password == null) || (password.trim().equals(""))){
            status = "12";
        } else{
           // factory().register(BusCompany.class);
            try{
                /*if(getBusCompany(email) != null ){
                    status = "13";
                } else{*/
                    busCompany = new BusCompany(email,password,companyName);

                    ofy().save().entity(busCompany);
                    /*CV cv = new CV();
                    cv.setContactDetail(new ContactDetail("","","","","","","","", ""));
                    member.setUserCV(new CV());
                    pm.makePersistent(member);*/

                    status = "ok";
                //}

            } catch(Exception e){
                System.out.println(e);
                status = "14";
            } finally{
                //ofy().clear();
                System.out.println("got here");
            }
        }

        return status;
    }
}