package com.badmus.southampton.busstop.utility;

import com.badmus.southampton.busstop.model.BusCompany;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 * Created by Badmus on 04/01/2016.
 */
public class OfyService {

    static {
        ObjectifyService.register(BusCompany.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {

        return ObjectifyService.factory();
    }
}
