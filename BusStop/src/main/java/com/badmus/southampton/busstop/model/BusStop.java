package com.badmus.southampton.busstop.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.List;

/**
 * Created by Badmus on 02/01/2016.
 */
@Entity
public class BusStop {

    @Id
    private String name;
    private String location;
    private List<BusTime> busTimes;
    private String message;
}
